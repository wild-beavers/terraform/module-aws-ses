variable "tags" {
  description = "Map of tags that will be shared with all the resources of this module."
  type        = map(string)
  default     = {}

  nullable = false
}

variable "account_vdm_dashboard_engagement_metrics" {
  description = "Whether to enable the VDM dashboard engagement metrics at account level."
  type        = bool
  default     = false

  nullable = false
}

variable "account_vdm_guardian_optimized_shared_delivery" {
  description = "Whether to enable the VDM guardian optimized shared delivery at the account level."
  type        = bool
  default     = false

  nullable = false
}

variable "account_suppression_attributes" {
  description = "A list that contains the reasons that email addresses will be automatically added to the suppression list for your account. Valid values: `COMPLAINT`, `BOUNCE`."
  type        = list(string)
  default     = []
  nullable    = false

  validation {
    condition = length(var.account_suppression_attributes) == 0 || alltrue([for attribute in var.account_suppression_attributes :
      contains(["COMPLAINT", "BOUNCE"], attribute)
    ])
    error_message = "Allowed values for “var.account_suppression_attributes” are `COMPLAINT` or `BOUNCE`."
  }
}

variable "configuration_set_enabled" {
  description = "Whether to create configuration set."
  type        = bool
  default     = false

  nullable = false
}

variable "configuration_set" {
  description = <<-DOCUMENTATION
Object representing the configuration set configuration.
  * name (required, string): the name of the configuration set.
  * max_delivery_seconds (optional, number, 300): The maximum amount of time, in seconds, that Amazon SES API v2 will attempt delivery of email. The value must greater than or equal to 300 seconds (5 minutes) and less than or equal to 50400 seconds (840 minutes).
  * tls_required (optional, bool, false): whether messages that use the configuration set are required to use Transport Layer Security (TLS)
  * reputation_metrics_enabled (optional, bool, false): whether to enable tracking of reputation metrics
  * sending_enabled (optional, bool, false): whether to enable email sending
  * suppression_options (optional, list(string), false): a list that contains the reasons that email addresses are automatically added to the suppression list for your account
  * tracking_options_custom_redirect_domain (optional, string, ""): the domain to use for tracking open and click events
  * vdm_dashboard_engagement_metrics (optional, bool, false): whether to enable the VDM dashboard engagement metrics for the configuration set
  * vdm_guardian_optimized_shared_delivery (optional, bool, false): whether to enable the VDM guardian optimized shared delivery for the configuration set
  * tags (optional, map(string), {}): map of tags to be apply to the configuration set. Will be merged with “var.tags”
DOCUMENTATION
  type = object({
    name                                    = string
    max_delivery_seconds                    = optional(number, 300)
    tls_required                            = optional(bool, false)
    reputation_metrics_enabled              = optional(bool, false)
    sending_enabled                         = optional(bool, false)
    suppression_options                     = optional(list(string), [])
    tracking_options_custom_redirect_domain = optional(string, "")
    vdm_dashboard_engagement_metrics        = optional(bool, false)
    vdm_guardian_optimized_shared_delivery  = optional(bool, false)
    tags                                    = optional(map(string), {})
  })
  default  = { name = "" }
  nullable = false

  validation {
    condition     = alltrue([for v in var.configuration_set.suppression_options : contains(["BOUNCE", "COMPLAINT"], v)])
    error_message = "“var.configuration_set.suppression_options” contains one or more invalid values. Possible values are “BOUNCE” or “COMPLAINT”."
  }
  validation {
    condition     = var.configuration_set.tracking_options_custom_redirect_domain == "" || can(regex("^([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}\\.)+[a-zA-Z]{2,}$", var.configuration_set.tracking_options_custom_redirect_domain))
    error_message = "“var.configuration_set.tracking_options_custom_redirect_domain” must match “^([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}\\.)+[a-zA-Z]{2,}$”"
  }
  validation {
    condition     = var.configuration_set.max_delivery_seconds >= 300 && var.configuration_set.max_delivery_seconds <= 50400
    error_message = "“var.configuration_set.max_delivery_seconds” must be between 300 and 50400."
  }
}

variable "configuration_set_event_destinations" {
  description = <<-DOCUMENTATION
  Map of configurations set event destinations. Keys are free values, values are:
 * name                       (required, string): the name of the configuration set event destination
 * matching_event_types (required, list(string)): list of event types. Valid values are “SEND”, “REJECT”, “BOUNCE”, “COMPLAINT”, “DELIVERY”, “OPEN”, “CLICK”, “RENDERING_FAILURE”, “DELIVERY_DELAY” or “SUBSCRIPTION”
 * cloudwatch (optional, object, {}): Object representing CloudWatch destination where:
   * default_dimension_value (required, string): the default value of the dimension that is published to Amazon CloudWatch if you don't provide the value of the dimension when you send an email.
   * dimension_name          (required, string): the name of an Amazon CloudWatch dimension associated with an email sending metric.
   * dimension_value_source  (required, string): the location where the Amazon SES finds the value of a dimension to publish to Amazon CloudWatch. Valid values are “MESSAGE_TAG”, “EMAIL_HEADER”, “LINK_TAG”
 * kinesis_firehose (optional, object, {}): Object representing Kinesis Firehose destination where:
   * delivery_stream_arn (required, string): The Amazon Resource Name (ARN) of the Amazon Kinesis Data Firehose stream that the Amazon SES API v2 sends email events to.
   * iam_role_arn        (required, string): The Amazon Resource Name (ARN) of the IAM role that the Amazon SES API v2 uses to send email events to the Amazon Kinesis Data Firehose stream.
 * pinpoint_application_arn (optional, string, ""): The Amazon Resource Name (ARN) of the Amazon Pinpoint project to send email events to.
 * sns_topic_arn (optional, string, ""): The Amazon Resource Name (ARN) of the Amazon SNS topic to publish email events to.
 * event_bus_arn (optional, string, ""): The Amazon Resource Name (ARN) of the Amazon EventBridge bus to publish email events to. Only the default bus is supported.
DOCUMENTATION
  type = map(object({
    name                 = string
    matching_event_types = list(string)
    cloudwatch = optional(object({
      default_dimension_value = string
      dimension_name          = string
      dimension_value_source  = string
    }))
    kinesis_firehose = optional(object({
      delivery_stream_arn = string
      iam_role_arn        = string
    }))
    pinpoint_application_arn = optional(string, "")
    sns_topic_arn            = optional(string, "")
    event_bus_arn            = optional(string, "")
  }))
  default = {}

  nullable = false

  validation {
    condition     = alltrue([for k, v in var.configuration_set_event_destinations : contains(["SEND", "REJECT", "BOUNCE", "COMPLAINT", "DELIVERY", "OPEN", "CLICK", "RENDERING_FAILURE", "DELIVERY_DELAY", "SUBSCRIPTION"], v.matching_event_types)])
    error_message = "One of more element of “var.configuration_set_event_destinations” are invalid. Check the requirements in the variables.tf file."
  }
}

variable "contact_list_enabled" {
  description = "Whether to enable SES contact list"
  type        = bool
  default     = false

  nullable = false
}

variable "contact_list" {
  description = <<-DOCUMENTATION
Object representing a contact list.
  * name (required, string): name of the contact list
  * description (optional, string): description of what the contact list is about
  * topic: Object representing a topic:
    * enabled (optional, bool, false): whether to enable contact list topic
    * default_subscription_status (optional, string): default subscription status to be applied to a contact if the contact has not noted their preference for subscribing to a topic
    * display_name (optional, string): name of the topic the contact will see
    * name (required, string): name of the topic
    * description (optional, string): description of what the topic is about, which the contact will see
  * tags (optional, map(string), {}): ap of tags to apply to contact list. Will be merged with “var.tags”
DOCUMENTATION
  type = object({
    name        = string
    description = optional(string)
    topic = optional(object({
      enabled                     = optional(bool, false)
      default_subscription_status = optional(string)
      display_name                = optional(string)
      name                        = optional(string)
      description                 = optional(string)
    }))
    tags = optional(map(string), {})
  })
  default = { name = "" }
}

variable "dedicated_ip_pool_enabled" {
  description = "Whether to create a dedicated pool IP."
  type        = bool
  default     = false

  nullable = false
}

variable "dedicated_ip_pool" {
  description = <<-DOCUMENTATION
Object representing a dedicated IP pool
  * name (required, string): name of the dedicated IP pool
  * scaling_mode (optional, string): IP pool scaling mode. Valid values are “STANDARD” or “MANAGED”. If omitted, the AWS API will default to a standard pool
  * tags (optional, map(string), {}): tags to apply to the dedicated IP pool resource. Will be merged with “var.tags”
DOCUMENTATION
  type = object({
    name         = string
    scaling_mode = optional(string)
    tags         = optional(map(string), {})
  })
  default = { name = "" }

  validation {
    condition     = var.dedicated_ip_pool.scaling_mode == null || contains(["STANDARD", "MANAGED"], coalesce(var.dedicated_ip_pool.scaling_mode, "failed"))
    error_message = "“var.dedicated_ip_pool.scaling_mode” must be one of “STANDARD” or “MANAGED”"
  }
}

variable "dedicated_ip_pool_assignment_enabled" {
  description = "Whether to assign an IP from the created IP pool"
  type        = bool
  default     = false

  nullable = false
}

variable "dedicated_ip_pool_assignment_ip" {
  description = "Dedicated IP address."
  type        = string
  default     = null

  validation {
    condition     = var.dedicated_ip_pool_assignment_ip == null || can(cidrhost("${var.dedicated_ip_pool_assignment_ip}/32", 0))
    error_message = "“var.dedicated_ip_pool_assignment_ip” must be a valid IPv4 or IPv6."
  }
}

variable "dkim_signing_enabled" {
  description = "Whether to enable DKIM signing"
  type        = bool
  default     = false

  nullable = false
}

variable "dkim" {
  description = <<-DOCUMENTATION
An object that describe the DKIM configuration.
  * domain_signing_private_key (optional, string): [Bring Your Own DKIM] A private key that's used to generate a DKIM signature. The private key must use 1024 or 2048-bit RSA encryption, and must be encoded using base64 encoding
  * domain_signing_selector (optional, string): [Bring Your Own DKIM] A string that's used to identify a public key in the DNS configuration for a domain
  * next_signing_key_length (optional, string): [Easy DKIM] The key length of the future DKIM key pair to be generated. This can be changed at most once per day. Valid values are “RSA_1024_BIT” or “RSA_2048_BIT”
  * route53_records_enabled (optional, bool, false): whether to create DKIM route53 records
DOCUMENTATION
  type = object({
    domain_signing_private_key = optional(string)
    domain_signing_selector    = optional(string)
    next_signing_key_length    = optional(string)
    route53_records_enabled    = optional(bool, false)
  })
  default = {}

  validation {
    condition     = var.dkim.domain_signing_private_key == null || can(base64decode(var.dkim.domain_signing_private_key))
    error_message = "“var.dkim.domain_signing_private_key” must be a base64 encoded private key."
  }

  validation {
    condition     = var.dkim.next_signing_key_length == null || contains(["RSA_1024_BIT", "RSA_2048_BIT"], coalesce(var.dkim.next_signing_key_length, "invalid"))
    error_message = "“var.dkim.next_signing_key_length” must be one of “RSA_1024_BIT” or “RSA_2048_BIT”"
  }
}

variable "email_identity" {
  description = "The email address or domain to verify."
  type        = string
  default     = null

  validation {
    condition     = var.email_identity == null || can(regex("^([\\w\\-_+\\.]+\\@)?([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}\\.)+[a-zA-Z]{2,}$", coalesce(var.email_identity, "never-used")))
    error_message = "“var.email_identity” must match “^([\\w\\-_+\\.]+\\@)?([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}\\.)+[a-zA-Z]{2,}$”."
  }
}

variable "email_identity_tags" {
  description = "Map of tags to apply to the email validation resource. Will be merged with “var.tags”."
  type        = map(string)
  default     = {}

  nullable = false
}

variable "email_identity_tags_feedback_forwarding_enabled" {
  description = "Whether to set the feedback forwarding configuration for the identity."
  type        = bool
  default     = false

  nullable = false
}

variable "email_identity_mail_from_attributes_enabled" {
  description = "Whether to enable the Email From Attributes."
  type        = bool
  default     = false

  nullable = false
}

variable "email_identity_mail_from_attributes_behavior_on_mx_failure" {
  description = "The action to take if the required MX record isn't found when you send an email. Valid values are “USE_DEFAULT_VALUE”, “REJECT_MESSAGE”."
  type        = string
  default     = "USE_DEFAULT_VALUE"

  nullable = false

  validation {
    condition     = contains(["USE_DEFAULT_VALUE", "REJECT_MESSAGE"], var.email_identity_mail_from_attributes_behavior_on_mx_failure)
    error_message = "“var.email_identity_mail_from_attributes_behavior_on_mx_failure” must be one of “USE_DEFAULT_VALUE” or “REJECT_MESSAGE”"
  }
}

variable "email_identity_mail_from_attributes_mail_from_domain" {
  description = "The custom MAIL FROM domain that you want the verified identity to use. Required if behavior_on_mx_failure is “REJECT_MESSAGE”."
  type        = string
  default     = null

  validation {
    condition     = var.email_identity_mail_from_attributes_mail_from_domain == null || can(regex("^([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}\\.)+[a-zA-Z]{2,}$", var.email_identity_mail_from_attributes_mail_from_domain))
    error_message = "“var.email_identity_mail_from_attributes_mail_from_domain” must match “^([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}\\.)+[a-zA-Z]{2,}$”"
  }
}

variable "email_identity_mail_spf_route53_records_enabled" {
  description = "Whether to create SPF route53 records."
  type        = bool
  default     = false

  nullable = false
}

variable "dmarc_route53_records_enabled" {
  description = "Whether to create DMARC route53 records."
  type        = bool
  default     = false

  nullable = false
}

variable "dmarc_policy" {
  description = "The DMARC policy. This will prefix with “v=DMARC1;”."
  type        = string
  default     = "p=none;"

  nullable = false
}

variable "route53_zone_id" {
  description = "Route53 zone id where to create route53 SPF, DKIM and DMARC records."
  type        = string
  default     = ""

  nullable = false

  validation {
    condition     = var.route53_zone_id == "" || can(regex("^Z([A-Z0-9]{19,20}|[A-Z0-9]{12})$", var.route53_zone_id))
    error_message = "“var.route53_zone_id” must match “^Z([A-Z0-9]{19,20}|[A-Z0-9]{12})$”."
  }
}

variable "iam_user_enabled" {
  description = "Whether the IAM user for SES."
  type        = bool
  default     = false

  nullable = false
}

variable "iam_user" {
  description = <<-DOCUMENTATION
Object representing the SES IAM user
  * name (required, string): name of the SES IAM user
  * policy_name (required, string): name of the policy of the IAM user for SES
  * path (optional, string, "/"): path in which to create the user
  * force_destroy (optional, bool, false): when destroying this user, destroy even if it has non-Terraform-managed IAM access keys, login profile or MFA devices.
  * tags (optional, map(string), {}): map of tags to be applied to this IAM user. Will be merged with “var.tags”
  * policy_source_policy_documents (optional, list(string), []): list of IAM policy JSON documents that are merged with the SES IAM user policy
  * access_key_enabled (optional, bool, false): whether to create the username/password for SES user created with “var.iam_user_enabled”
  * access_key_pgp_key (optional, string): either a base-64 encoded PGP public key, or a keybase username in the form “keybase:some_person_that_exists”
  * ses_allowed_from_addresses (optional, list(string), ["*"]): list of email addresses allowed to be impersonate by this user
  * ses_allowed_recipients (optional, list(string), ["*"]): list of allowed email address recipients for this user

DOCUMENTATION
  type = object({
    name                           = string
    policy_name                    = string
    path                           = optional(string, "/")
    force_destroy                  = optional(bool, false)
    tags                           = optional(map(string), {})
    policy_source_policy_documents = optional(list(string), [])
    access_key_enabled             = optional(bool, false)
    access_key_pgp_key             = optional(string)
    ses_allowed_from_addresses     = optional(list(string), ["*"])
    ses_allowed_recipients         = optional(list(string), ["*"])
  })
  default = {
    name        = "",
    policy_name = ""
  }

  validation {
    condition     = var.iam_user.name == "" || can(regex("^[A-Za-z0-9+=,\\.@_-]{1,64}$", var.iam_user.name))
    error_message = "“var.iam_user.name” must match “^[A-Za-z0-9+=,\\.@_-]{1,64}$”"
  }
  validation {
    condition     = can(regex("^/([A-Za-z0-9+=,\\.@_-]+/)*$", var.iam_user.path))
    error_message = "“var.iam_user.path” must match “^/([A-Za-z0-9+=,\\.@_-]+/)*$”."
  }
  validation {
    condition     = var.iam_user.policy_name == "" || can(regex("^[A-Za-z0-9+=,\\.@_-]{1,128}$", var.iam_user.policy_name))
    error_message = "“var.iam_user.policy_name” must match “^[A-Za-z0-9+=,\\.@_-]{1,128}$”"
  }

  validation {
    condition     = alltrue([for policy in var.iam_user.policy_source_policy_documents : can(jsondecode(policy))])
    error_message = "One or more element of “var.iam_user.source_policy_documents” are invalid JSON document."
  }
}
