## 3.0.0

- maintenance: (BREAKING) pin minimal version of AWS provider to `5.81`
- feat: adds `var.account_suppression_attributes`
- fix: adds new `var.configuration_set.max_delivery_seconds`, default to `300` to workaround issue [40591](https://github.com/hashicorp/terraform-provider-aws/issues/40591)

## 2.0.0

- refactor: (BREAKING) changes outputs to new standards:
   - `configuration_set_arn` => `aws_sesv2_configuration_set.arn`
   - `contact_list_id` => `aws_sesv2_contact_list.id`
   - `dedicated_ip_pool_id` => `aws_sesv2_dedicated_ip_pool.id`
   - `dedicated_ip_assignment_id` => *removed*
   - `email_identity_arn` => `aws_sesv2_email_identity.arn`
   - `email_identity_type` => `aws_sesv2_email_identity.identity_type`
   - `email_identity_dkim_signing_attributes_token` => *removed*
   - `iam_user_arn` => `aws_iam_user.arn`
   - `iam_user_id` => `aws_iam_user.id`
   - `iam_user_unique_id` => `aws_iam_user.unique_id`
   - `iam_user_access_key_ses_smtp_username_v4` => `aws_iam_access_key.id`
   - `iam_user_access_key_encrypted_ses_smtp_password_v4` => `aws_iam_access_key.encrypted_ses_smtp_password_v4`
   - `iam_user_access_key_ses_smtp_password_v4` => *removed*
- fix: makes `origin` tag not overridable by `var.tags`.
- chore: bump pre-commit hooks + add markdown validation

## 1.0.1

- fix: wrong ARN format on SES user policy

## 1.0.0

- feat: init
- chore: bump pre-commit hooks

## 0.0.0

- tech: initial version
