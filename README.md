# SES Terraform Module

Terraform module to create SES resources.
It also can append route53 records for DKIM and SPF validation.
It also can create a IAM user for SMTP credentials.

> Briefly describe what this project is about

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 1.9 |
| aws | >= 5.81 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 5.81 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_access_key.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_access_key) | resource |
| [aws_iam_user.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user) | resource |
| [aws_iam_user_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user_policy) | resource |
| [aws_route53_record.dkim](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_record.dmarc](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_record.spf](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_sesv2_account_suppression_attributes.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sesv2_account_suppression_attributes) | resource |
| [aws_sesv2_account_vdm_attributes.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sesv2_account_vdm_attributes) | resource |
| [aws_sesv2_configuration_set.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sesv2_configuration_set) | resource |
| [aws_sesv2_configuration_set_event_destination.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sesv2_configuration_set_event_destination) | resource |
| [aws_sesv2_contact_list.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sesv2_contact_list) | resource |
| [aws_sesv2_dedicated_ip_assignment.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sesv2_dedicated_ip_assignment) | resource |
| [aws_sesv2_dedicated_ip_pool.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sesv2_dedicated_ip_pool) | resource |
| [aws_sesv2_email_identity.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sesv2_email_identity) | resource |
| [aws_sesv2_email_identity_feedback_attributes.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sesv2_email_identity_feedback_attributes) | resource |
| [aws_sesv2_email_identity_mail_from_attributes.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/sesv2_email_identity_mail_from_attributes) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_partition.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/partition) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| account\_suppression\_attributes | A list that contains the reasons that email addresses will be automatically added to the suppression list for your account. Valid values: `COMPLAINT`, `BOUNCE`. | `list(string)` | `[]` | no |
| account\_vdm\_dashboard\_engagement\_metrics | Whether to enable the VDM dashboard engagement metrics at account level. | `bool` | `false` | no |
| account\_vdm\_guardian\_optimized\_shared\_delivery | Whether to enable the VDM guardian optimized shared delivery at the account level. | `bool` | `false` | no |
| configuration\_set | Object representing the configuration set configuration.<br/>  * name (required, string): the name of the configuration set.<br/>  * max\_delivery\_seconds (optional, number, 300): The maximum amount of time, in seconds, that Amazon SES API v2 will attempt delivery of email. The value must greater than or equal to 300 seconds (5 minutes) and less than or equal to 50400 seconds (840 minutes).<br/>  * tls\_required (optional, bool, false): whether messages that use the configuration set are required to use Transport Layer Security (TLS)<br/>  * reputation\_metrics\_enabled (optional, bool, false): whether to enable tracking of reputation metrics<br/>  * sending\_enabled (optional, bool, false): whether to enable email sending<br/>  * suppression\_options (optional, list(string), false): a list that contains the reasons that email addresses are automatically added to the suppression list for your account<br/>  * tracking\_options\_custom\_redirect\_domain (optional, string, ""): the domain to use for tracking open and click events<br/>  * vdm\_dashboard\_engagement\_metrics (optional, bool, false): whether to enable the VDM dashboard engagement metrics for the configuration set<br/>  * vdm\_guardian\_optimized\_shared\_delivery (optional, bool, false): whether to enable the VDM guardian optimized shared delivery for the configuration set<br/>  * tags (optional, map(string), {}): map of tags to be apply to the configuration set. Will be merged with “var.tags” | <pre>object({<br/>    name                                    = string<br/>    max_delivery_seconds                    = optional(number, 300)<br/>    tls_required                            = optional(bool, false)<br/>    reputation_metrics_enabled              = optional(bool, false)<br/>    sending_enabled                         = optional(bool, false)<br/>    suppression_options                     = optional(list(string), [])<br/>    tracking_options_custom_redirect_domain = optional(string, "")<br/>    vdm_dashboard_engagement_metrics        = optional(bool, false)<br/>    vdm_guardian_optimized_shared_delivery  = optional(bool, false)<br/>    tags                                    = optional(map(string), {})<br/>  })</pre> | <pre>{<br/>  "name": ""<br/>}</pre> | no |
| configuration\_set\_enabled | Whether to create configuration set. | `bool` | `false` | no |
| configuration\_set\_event\_destinations | Map of configurations set event destinations. Keys are free values, values are:<br/>* name                       (required, string): the name of the configuration set event destination<br/>* matching\_event\_types (required, list(string)): list of event types. Valid values are “SEND”, “REJECT”, “BOUNCE”, “COMPLAINT”, “DELIVERY”, “OPEN”, “CLICK”, “RENDERING\_FAILURE”, “DELIVERY\_DELAY” or “SUBSCRIPTION”<br/>* cloudwatch (optional, object, {}): Object representing CloudWatch destination where:<br/>  * default\_dimension\_value (required, string): the default value of the dimension that is published to Amazon CloudWatch if you don't provide the value of the dimension when you send an email.<br/>  * dimension\_name          (required, string): the name of an Amazon CloudWatch dimension associated with an email sending metric.<br/>  * dimension\_value\_source  (required, string): the location where the Amazon SES finds the value of a dimension to publish to Amazon CloudWatch. Valid values are “MESSAGE\_TAG”, “EMAIL\_HEADER”, “LINK\_TAG”<br/>* kinesis\_firehose (optional, object, {}): Object representing Kinesis Firehose destination where:<br/>  * delivery\_stream\_arn (required, string): The Amazon Resource Name (ARN) of the Amazon Kinesis Data Firehose stream that the Amazon SES API v2 sends email events to.<br/>  * iam\_role\_arn        (required, string): The Amazon Resource Name (ARN) of the IAM role that the Amazon SES API v2 uses to send email events to the Amazon Kinesis Data Firehose stream.<br/>* pinpoint\_application\_arn (optional, string, ""): The Amazon Resource Name (ARN) of the Amazon Pinpoint project to send email events to.<br/>* sns\_topic\_arn (optional, string, ""): The Amazon Resource Name (ARN) of the Amazon SNS topic to publish email events to.<br/>* event\_bus\_arn (optional, string, ""): The Amazon Resource Name (ARN) of the Amazon EventBridge bus to publish email events to. Only the default bus is supported. | <pre>map(object({<br/>    name                 = string<br/>    matching_event_types = list(string)<br/>    cloudwatch = optional(object({<br/>      default_dimension_value = string<br/>      dimension_name          = string<br/>      dimension_value_source  = string<br/>    }))<br/>    kinesis_firehose = optional(object({<br/>      delivery_stream_arn = string<br/>      iam_role_arn        = string<br/>    }))<br/>    pinpoint_application_arn = optional(string, "")<br/>    sns_topic_arn            = optional(string, "")<br/>    event_bus_arn            = optional(string, "")<br/>  }))</pre> | `{}` | no |
| contact\_list | Object representing a contact list.<br/>  * name (required, string): name of the contact list<br/>  * description (optional, string): description of what the contact list is about<br/>  * topic: Object representing a topic:<br/>    * enabled (optional, bool, false): whether to enable contact list topic<br/>    * default\_subscription\_status (optional, string): default subscription status to be applied to a contact if the contact has not noted their preference for subscribing to a topic<br/>    * display\_name (optional, string): name of the topic the contact will see<br/>    * name (required, string): name of the topic<br/>    * description (optional, string): description of what the topic is about, which the contact will see<br/>  * tags (optional, map(string), {}): ap of tags to apply to contact list. Will be merged with “var.tags” | <pre>object({<br/>    name        = string<br/>    description = optional(string)<br/>    topic = optional(object({<br/>      enabled                     = optional(bool, false)<br/>      default_subscription_status = optional(string)<br/>      display_name                = optional(string)<br/>      name                        = optional(string)<br/>      description                 = optional(string)<br/>    }))<br/>    tags = optional(map(string), {})<br/>  })</pre> | <pre>{<br/>  "name": ""<br/>}</pre> | no |
| contact\_list\_enabled | Whether to enable SES contact list | `bool` | `false` | no |
| dedicated\_ip\_pool | Object representing a dedicated IP pool<br/>  * name (required, string): name of the dedicated IP pool<br/>  * scaling\_mode (optional, string): IP pool scaling mode. Valid values are “STANDARD” or “MANAGED”. If omitted, the AWS API will default to a standard pool<br/>  * tags (optional, map(string), {}): tags to apply to the dedicated IP pool resource. Will be merged with “var.tags” | <pre>object({<br/>    name         = string<br/>    scaling_mode = optional(string)<br/>    tags         = optional(map(string), {})<br/>  })</pre> | <pre>{<br/>  "name": ""<br/>}</pre> | no |
| dedicated\_ip\_pool\_assignment\_enabled | Whether to assign an IP from the created IP pool | `bool` | `false` | no |
| dedicated\_ip\_pool\_assignment\_ip | Dedicated IP address. | `string` | `null` | no |
| dedicated\_ip\_pool\_enabled | Whether to create a dedicated pool IP. | `bool` | `false` | no |
| dkim | An object that describe the DKIM configuration.<br/>  * domain\_signing\_private\_key (optional, string): [Bring Your Own DKIM] A private key that's used to generate a DKIM signature. The private key must use 1024 or 2048-bit RSA encryption, and must be encoded using base64 encoding<br/>  * domain\_signing\_selector (optional, string): [Bring Your Own DKIM] A string that's used to identify a public key in the DNS configuration for a domain<br/>  * next\_signing\_key\_length (optional, string): [Easy DKIM] The key length of the future DKIM key pair to be generated. This can be changed at most once per day. Valid values are “RSA\_1024\_BIT” or “RSA\_2048\_BIT”<br/>  * route53\_records\_enabled (optional, bool, false): whether to create DKIM route53 records | <pre>object({<br/>    domain_signing_private_key = optional(string)<br/>    domain_signing_selector    = optional(string)<br/>    next_signing_key_length    = optional(string)<br/>    route53_records_enabled    = optional(bool, false)<br/>  })</pre> | `{}` | no |
| dkim\_signing\_enabled | Whether to enable DKIM signing | `bool` | `false` | no |
| dmarc\_policy | The DMARC policy. This will prefix with “v=DMARC1;”. | `string` | `"p=none;"` | no |
| dmarc\_route53\_records\_enabled | Whether to create DMARC route53 records. | `bool` | `false` | no |
| email\_identity | The email address or domain to verify. | `string` | `null` | no |
| email\_identity\_mail\_from\_attributes\_behavior\_on\_mx\_failure | The action to take if the required MX record isn't found when you send an email. Valid values are “USE\_DEFAULT\_VALUE”, “REJECT\_MESSAGE”. | `string` | `"USE_DEFAULT_VALUE"` | no |
| email\_identity\_mail\_from\_attributes\_enabled | Whether to enable the Email From Attributes. | `bool` | `false` | no |
| email\_identity\_mail\_from\_attributes\_mail\_from\_domain | The custom MAIL FROM domain that you want the verified identity to use. Required if behavior\_on\_mx\_failure is “REJECT\_MESSAGE”. | `string` | `null` | no |
| email\_identity\_mail\_spf\_route53\_records\_enabled | Whether to create SPF route53 records. | `bool` | `false` | no |
| email\_identity\_tags | Map of tags to apply to the email validation resource. Will be merged with “var.tags”. | `map(string)` | `{}` | no |
| email\_identity\_tags\_feedback\_forwarding\_enabled | Whether to set the feedback forwarding configuration for the identity. | `bool` | `false` | no |
| iam\_user | Object representing the SES IAM user<br/>  * name (required, string): name of the SES IAM user<br/>  * policy\_name (required, string): name of the policy of the IAM user for SES<br/>  * path (optional, string, "/"): path in which to create the user<br/>  * force\_destroy (optional, bool, false): when destroying this user, destroy even if it has non-Terraform-managed IAM access keys, login profile or MFA devices.<br/>  * tags (optional, map(string), {}): map of tags to be applied to this IAM user. Will be merged with “var.tags”<br/>  * policy\_source\_policy\_documents (optional, list(string), []): list of IAM policy JSON documents that are merged with the SES IAM user policy<br/>  * access\_key\_enabled (optional, bool, false): whether to create the username/password for SES user created with “var.iam\_user\_enabled”<br/>  * access\_key\_pgp\_key (optional, string): either a base-64 encoded PGP public key, or a keybase username in the form “keybase:some\_person\_that\_exists”<br/>  * ses\_allowed\_from\_addresses (optional, list(string), ["*"]): list of email addresses allowed to be impersonate by this user<br/>  * ses\_allowed\_recipients (optional, list(string), ["*"]): list of allowed email address recipients for this user | <pre>object({<br/>    name                           = string<br/>    policy_name                    = string<br/>    path                           = optional(string, "/")<br/>    force_destroy                  = optional(bool, false)<br/>    tags                           = optional(map(string), {})<br/>    policy_source_policy_documents = optional(list(string), [])<br/>    access_key_enabled             = optional(bool, false)<br/>    access_key_pgp_key             = optional(string)<br/>    ses_allowed_from_addresses     = optional(list(string), ["*"])<br/>    ses_allowed_recipients         = optional(list(string), ["*"])<br/>  })</pre> | <pre>{<br/>  "name": "",<br/>  "policy_name": ""<br/>}</pre> | no |
| iam\_user\_enabled | Whether the IAM user for SES. | `bool` | `false` | no |
| route53\_zone\_id | Route53 zone id where to create route53 SPF, DKIM and DMARC records. | `string` | `""` | no |
| tags | Map of tags that will be shared with all the resources of this module. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| aws\_iam\_access\_key | n/a |
| aws\_iam\_user | n/a |
| aws\_ses\_server\_domain\_name | n/a |
| aws\_sesv2\_configuration\_set | n/a |
| aws\_sesv2\_contact\_list | n/a |
| aws\_sesv2\_dedicated\_ip\_pool | n/a |
| aws\_sesv2\_email\_identity | n/a |
| precomputed\_aws\_iam\_user | n/a |
<!-- END_TF_DOCS -->
