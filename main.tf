locals {
  tags = merge(
    var.tags,
    {
      managed-by = "terraform"
      origin     = "https://gitlab.com/wild-beavers/terraform/module-aws-ses"
    },
  )
  aws_ses_domain = "amazonses.com"
  # As of 2024-05-06, the AWS provider doesn't offer a data source for that.
  aws_ses_server_domain_name = "email-smtp.${data.aws_region.current.name}.amazonaws.com"
}

data "aws_region" "current" {}
data "aws_partition" "current" {}
data "aws_caller_identity" "current" {}

resource "aws_sesv2_account_vdm_attributes" "this" {
  for_each = var.account_vdm_dashboard_engagement_metrics || var.account_vdm_guardian_optimized_shared_delivery ? { 0 = 0 } : {}

  vdm_enabled = "ENABLED"

  dashboard_attributes {
    engagement_metrics = var.account_vdm_dashboard_engagement_metrics ? "ENABLED" : "DISABLED"
  }

  guardian_attributes {
    optimized_shared_delivery = var.account_vdm_guardian_optimized_shared_delivery ? "ENABLED" : "DISABLED"
  }
}

resource "aws_sesv2_account_suppression_attributes" "this" {
  for_each = length(var.account_suppression_attributes) > 0 ? { 0 = 0 } : {}

  suppressed_reasons = var.account_suppression_attributes
}

resource "aws_sesv2_configuration_set" "this" {
  for_each = var.configuration_set_enabled ? { 0 = var.configuration_set } : {}

  configuration_set_name = each.value.name

  delivery_options {
    max_delivery_seconds = each.value.max_delivery_seconds
    sending_pool_name    = var.dedicated_ip_pool_enabled ? aws_sesv2_dedicated_ip_pool.this["0"].pool_name : null
    tls_policy           = each.value.tls_required ? "REQUIRE" : "OPTIONAL"
  }

  reputation_options {
    reputation_metrics_enabled = each.value.reputation_metrics_enabled
  }

  sending_options {
    sending_enabled = each.value.sending_enabled
  }

  dynamic "suppression_options" {
    for_each = length(each.value.suppression_options) > 0 ? { 0 = 0 } : {}
    content {
      suppressed_reasons = each.value.suppression_options
    }
  }

  dynamic "tracking_options" {
    for_each = each.value.tracking_options_custom_redirect_domain != "" ? { 0 = 0 } : {}

    content {
      custom_redirect_domain = each.value.tracking_options_custom_redirect_domain
    }
  }

  dynamic "vdm_options" {
    for_each = each.value.vdm_dashboard_engagement_metrics || each.value.vdm_guardian_optimized_shared_delivery ? { 0 = 0 } : {}

    content {
      dynamic "dashboard_options" {
        for_each = each.value.vdm_dashboard_engagement_metrics ? { 0 = 0 } : {}

        content {
          engagement_metrics = each.value.vdm_dashboard_engagement_metrics ? "ENABLED" : "DISABLED"
        }
      }

      dynamic "guardian_options" {
        for_each = each.value.vdm_guardian_optimized_shared_delivery ? { 0 = 0 } : {}

        content {
          optimized_shared_delivery = each.value.vdm_guardian_optimized_shared_delivery ? "ENABLED" : "DISABLED"
        }
      }
    }
  }

  tags = merge(local.tags, each.value.tags)

  lifecycle {
    precondition {
      condition     = var.configuration_set_enabled && each.value.name != ""
      error_message = "“var.configuration_set.name” must be set when “var.configuration_set_enabled” is enabled."
    }
  }
}

resource "aws_sesv2_configuration_set_event_destination" "this" {
  for_each = var.configuration_set_enabled && length(var.configuration_set_event_destinations) > 0 ? var.configuration_set_event_destinations : {}

  configuration_set_name = aws_sesv2_configuration_set.this["0"].configuration_set_name
  event_destination_name = each.value.name

  event_destination {
    enabled              = true
    matching_event_types = each.value.matching_event_types

    dynamic "cloud_watch_destination" {
      for_each = each.value.cloudwatch

      content {
        dimension_configuration {
          default_dimension_value = cloud_watch_destination.value.default_dimension_value
          dimension_name          = cloud_watch_destination.value.dimension_name
          dimension_value_source  = cloud_watch_destination.value.dimension_value_source
        }
      }
    }

    dynamic "kinesis_firehose_destination" {
      for_each = each.value.kinesis_firehose

      content {
        delivery_stream_arn = kinesis_firehose_destination.value.delivery_stream_arn
        iam_role_arn        = kinesis_firehose_destination.value.iam_role_arn
      }
    }

    dynamic "pinpoint_destination" {
      for_each = each.value.pinpoint_application_arn != "" ? { 0 = 0 } : {}

      content {
        application_arn = each.value.pinpoint_application_arn
      }
    }

    dynamic "sns_destination" {
      for_each = each.value.sns_topic_arn != "" ? { 0 = 0 } : {}

      content {
        topic_arn = each.value.sns_topic_arn
      }
    }

    dynamic "event_bridge_destination" {
      for_each = each.value.event_bus_arn != "" ? { 0 = 0 } : {}

      content {
        event_bus_arn = each.value.event_bus_arn
      }
    }
  }
}

resource "aws_sesv2_contact_list" "this" {
  for_each = var.contact_list_enabled ? { 0 = var.contact_list } : {}

  contact_list_name = each.value.name
  description       = each.value.description

  dynamic "topic" {
    for_each = each.value.topic.enabled ? { 0 = each.value.topic } : {}

    content {
      default_subscription_status = topic.value.default_subscription_status
      display_name                = topic.value.display_name
      topic_name                  = topic.value.name
      description                 = topic.value.description
    }
  }

  tags = merge(local.tags, each.value.tags)
}

resource "aws_sesv2_dedicated_ip_pool" "this" {
  for_each = var.dedicated_ip_pool_enabled ? { 0 = var.dedicated_ip_pool } : {}

  pool_name    = each.value.name
  scaling_mode = each.value.scaling_mode
  tags         = merge(local.tags, each.value.tags)
}

resource "aws_sesv2_dedicated_ip_assignment" "this" {
  for_each = var.dedicated_ip_pool_enabled && var.dedicated_ip_pool_assignment_enabled ? { 0 = 0 } : {}

  destination_pool_name = aws_sesv2_dedicated_ip_pool.this["0"].pool_name
  ip                    = var.dedicated_ip_pool_assignment_ip

  lifecycle {
    precondition {
      condition     = !(!var.dedicated_ip_pool_enabled && var.dedicated_ip_pool_assignment_enabled)
      error_message = "“var.dedicated_ip_pool_enabled” must be set when “var.dedicated_ip_pool_assignment_enabled” is enabled."
    }
  }
}

resource "aws_sesv2_email_identity" "this" {
  for_each = { 0 = 0 }

  email_identity = var.email_identity

  configuration_set_name = var.configuration_set_enabled ? aws_sesv2_configuration_set.this["0"].configuration_set_name : null

  dynamic "dkim_signing_attributes" {
    for_each = var.dkim_signing_enabled ? { 0 = var.dkim } : {}

    content {
      domain_signing_private_key = dkim_signing_attributes.value.domain_signing_private_key
      domain_signing_selector    = dkim_signing_attributes.value.domain_signing_selector
      next_signing_key_length    = dkim_signing_attributes.value.next_signing_key_length
    }
  }

  tags = merge(local.tags, var.email_identity_tags)

  lifecycle {
    ignore_changes = [dkim_signing_attributes]
  }
}

resource "aws_sesv2_email_identity_feedback_attributes" "this" {
  for_each = var.email_identity_tags_feedback_forwarding_enabled ? { 0 = 0 } : {}

  email_identity           = aws_sesv2_email_identity.this["0"].email_identity
  email_forwarding_enabled = var.email_identity_tags_feedback_forwarding_enabled
}

resource "aws_sesv2_email_identity_mail_from_attributes" "this" {
  for_each = var.email_identity_mail_from_attributes_enabled ? { 0 = 0 } : {}

  email_identity         = aws_sesv2_email_identity.this["0"].email_identity
  behavior_on_mx_failure = var.email_identity_mail_from_attributes_behavior_on_mx_failure
  mail_from_domain       = var.email_identity_mail_from_attributes_mail_from_domain

  lifecycle {
    precondition {
      condition     = !(var.email_identity_mail_from_attributes_behavior_on_mx_failure == "REJECT_MESSAGE" && var.email_identity_mail_from_attributes_mail_from_domain == null)
      error_message = "“var.email_identity_mail_from_attributes_mail_from_domain” must be set when “var.email_identity_mail_from_attributes_behavior_on_mx_failure” is “REJECT_MESSAGE”."
    }
  }
}

resource "aws_route53_record" "dkim" {
  # TF preprocessor can't determine there are 3 records to add.
  # The AWS documentation is clear that Easy DKIM requires 3 records
  # See https://docs.aws.amazon.com/ses/latest/dg/send-email-authentication-dkim-bring-your-own.html
  count = var.dkim_signing_enabled && var.dkim.route53_records_enabled ? 3 : 0

  zone_id = var.route53_zone_id
  name    = "${aws_sesv2_email_identity.this["0"].dkim_signing_attributes[0].tokens[count.index]}._domainkey.${var.email_identity}"
  type    = "CNAME"
  ttl     = 300
  records = [
    "${aws_sesv2_email_identity.this["0"].dkim_signing_attributes[0].tokens[count.index]}.dkim.${local.aws_ses_domain}"
  ]

  lifecycle {
    precondition {
      condition     = !(!var.dkim_signing_enabled && var.dkim.route53_records_enabled)
      error_message = "“var.dkim_signing_enabled” must be set when “var.dkim.route53_records_enabled”."
    }

    precondition {
      condition     = !(var.dkim.route53_records_enabled && var.route53_zone_id == "")
      error_message = "“var.route53_zone_id” must be filled when “var.dkim.route53_records_enabled” is true."
    }

    precondition {
      condition     = !(var.dkim.route53_records_enabled && !can(regex("^([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}\\.)+[a-zA-Z]{2,}$", coalesce(var.email_identity, "never-used"))))
      error_message = "“var.dkim.route53_records_enabled” is only compatible when “var.email_identity” is a domain name."
    }
  }
}

resource "aws_route53_record" "spf" {
  for_each = var.email_identity_mail_from_attributes_enabled && var.email_identity_mail_spf_route53_records_enabled ? {
    mx = {
      type    = "MX"
      records = ["10 feedback-smtp.${data.aws_region.current.name}.${local.aws_ses_domain}"]
    },
    spf = {
      type    = "TXT"
      records = ["v=spf1 include:${local.aws_ses_domain} ~all"]
    }
  } : {}

  zone_id = var.route53_zone_id
  name    = var.email_identity_mail_from_attributes_mail_from_domain
  type    = each.value.type
  ttl     = 300
  records = each.value.records

  lifecycle {
    precondition {
      condition     = !(!var.email_identity_mail_from_attributes_enabled && var.email_identity_mail_spf_route53_records_enabled)
      error_message = "“var.email_identity_mail_from_attributes_enabled” must be set when “var.email_identity_mail_spf_route53_records_enabled” is true."
    }

    precondition {
      condition     = !(var.email_identity_mail_spf_route53_records_enabled && var.email_identity_mail_from_attributes_mail_from_domain == "")
      error_message = "“var.email_identity_mail_from_attributes_mail_from_domain” can't be empty when “var.email_identity_mail_spf_route53_records_enabled” is set."
    }

    precondition {
      condition     = !(var.email_identity_mail_spf_route53_records_enabled && var.route53_zone_id == "")
      error_message = "“var.route53_zone_id” must be filled when “var.email_identity_mail_spf_route53_records_enabled” is true."
    }

    precondition {
      condition     = !(var.email_identity_mail_spf_route53_records_enabled && !can(regex("^([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}\\.)+[a-zA-Z]{2,}$", coalesce(var.email_identity, "never-used"))))
      error_message = "“var.email_identity_mail_spf_route53_records_enabled” is only compatible when “var.email_identity” is a domain name."
    }
  }
}

resource "aws_route53_record" "dmarc" {
  for_each = var.dmarc_route53_records_enabled ? { 0 = 0 } : {}

  zone_id = var.route53_zone_id
  name    = "_dmarc.${var.email_identity}"
  type    = "TXT"
  ttl     = 300
  records = ["v=DMARC1; ${var.dmarc_policy}"]

  lifecycle {
    precondition {
      condition     = !(var.dmarc_route53_records_enabled && var.route53_zone_id == "")
      error_message = "“var.route53_zone_id” must be filled when “var.dmarc_route53_records_enabled” is true."
    }

    precondition {
      condition     = !(var.dmarc_route53_records_enabled && !can(regex("^([a-zA-Z0-9][a-zA-Z0-9-_]{1,61}\\.)+[a-zA-Z]{2,}$", coalesce(var.email_identity, "never-used"))))
      error_message = "“var.dmarc_route53_records_enabled” is only compatible when “var.email_identity” is a domain name."
    }
  }
}

resource "aws_iam_user" "this" {
  for_each = var.iam_user_enabled ? { 0 = 0 } : {}

  name          = var.iam_user.name
  path          = var.iam_user.path
  force_destroy = var.iam_user.force_destroy

  tags = merge(local.tags, var.iam_user.tags)
}

resource "aws_iam_user_policy" "this" {
  for_each = var.iam_user_enabled ? { 0 = 0 } : {}

  policy = data.aws_iam_policy_document.this["0"].json
  user   = aws_iam_user.this["0"].name
  name   = var.iam_user.policy_name
}

data "aws_iam_policy_document" "this" {
  for_each = var.iam_user_enabled ? { 0 = 0 } : {}

  source_policy_documents = var.iam_user.policy_source_policy_documents

  statement {
    sid     = "SendEmail"
    effect  = "Allow"
    actions = ["ses:SendRawEmail"]
    resources = compact(concat(
      [format("arn:%s:ses:%s:%s:identity/*", data.aws_partition.current.partition, data.aws_region.current.name, data.aws_caller_identity.current.account_id)],
      var.configuration_set_enabled ? [aws_sesv2_configuration_set.this["0"].arn] : []
    ))
    condition {
      test     = "StringLike"
      variable = "ses:FromAddress"
      values   = var.iam_user.ses_allowed_from_addresses
    }
    condition {
      test     = "ForAllValues:StringLike"
      variable = "ses:Recipients"
      values   = var.iam_user.ses_allowed_recipients
    }
  }
}

resource "aws_iam_access_key" "this" {
  for_each = var.iam_user_enabled && var.iam_user.access_key_enabled ? { 0 = 0 } : {}

  user    = aws_iam_user.this["0"].name
  status  = "Active"
  pgp_key = var.iam_user.access_key_pgp_key
}
