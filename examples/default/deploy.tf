####
# Randoms
####

resource "random_string" "this" {
  length  = 3
  upper   = false
  special = false
  numeric = true
}

resource "random_string" "start" {
  length  = 1
  numeric = false
  special = false
  upper   = false
}

####
# Data
####

data "aws_route53_zone" "test" {
  name = local.dns_suffix
}

####
# Locals
####

locals {
  prefix     = "${random_string.start.result}${random_string.this.result}"
  dns_suffix = "lb.anuvu.cloud"
}

####
# Default
####

module "simple_email" {
  source = "../.."

  email_identity = "${local.prefix}tftest@${local.dns_suffix}"
}

module "simple_domain" {
  source = "../.."

  email_identity = "${local.prefix}tftest.${local.dns_suffix}"
}

####
# Complete
#  - Except dedicated IP which requires a complex environment to test
####

module "complete" {
  source = "../.."

  email_identity = "${local.prefix}tftestcomplete.${local.dns_suffix}"

  configuration_set_enabled = true
  configuration_set = {
    reputation_metrics_enabled             = true
    name                                   = "${local.prefix}tftestcomplete"
    max_delivery_seconds                   = 450
    sending_enabled                        = true
    suppression_options                    = ["BOUNCE"]
    tls_required                           = true
    tags                                   = { my-tag = "foo" }
    vdm_dashboard_engagement_metrics       = true
    vdm_guardian_optimized_shared_delivery = true
  }

  contact_list_enabled = true
  contact_list = {
    enabled     = true
    name        = "${local.prefix}tftestcomplete"
    description = "This is superb description. U_u"
    topic = {
      enabled                     = true
      description                 = "I'm a basic contact list. Pass you way."
      default_subscription_status = "OPT_IN"
      display_name                = "${local.prefix}tftestcomplete"
      name                        = "${local.prefix}tftestcomplete"
    }
  }

  dkim_signing_enabled = true
  dkim = {
    next_signing_key_length = "RSA_2048_BIT"
    route53_records_enabled = true
  }

  dmarc_route53_records_enabled = true
  dmarc_policy                  = "p=quarantine;rua=mailto:report@${"${local.prefix}tftestcomplete.${local.dns_suffix}"}"

  email_identity_mail_from_attributes_enabled                = true
  email_identity_mail_from_attributes_behavior_on_mx_failure = "REJECT_MESSAGE"
  email_identity_mail_from_attributes_mail_from_domain       = "mail.${local.prefix}tftestcomplete.${local.dns_suffix}"
  email_identity_mail_spf_route53_records_enabled            = true
  email_identity_tags_feedback_forwarding_enabled            = true

  iam_user_enabled = true
  iam_user = {
    name               = "${local.prefix}tftestcomplete"
    force_destroy      = true
    path               = "/smtp/"
    policy_name        = "${local.prefix}tftestcomplete"
    access_key_enabled = true
    tags               = { my-smtp-user = "bar" }
  }

  route53_zone_id = data.aws_route53_zone.test.id

  tags = { tftest = true }
}

#####
# Account level settings
# Example only - disabled because cannot be parallelized
#####

#    module "account_level_access" {
#      source = "../../"
#
#      account_vdm_dashboard_engagement_metrics = true
#      account_vdm_guardian_optimized_shared_delivery = true
#      account_suppression_attributes = ["BOUNCE"]
#    }
