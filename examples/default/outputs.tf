output "simple_email" {
  value = module.simple_email
}

output "simple_domain" {
  value = module.simple_domain
}

output "complete" {
  value = module.complete
}
