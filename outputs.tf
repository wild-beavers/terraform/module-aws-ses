output "aws_sesv2_configuration_set" {
  value = var.configuration_set_enabled ? { for k, v in aws_sesv2_configuration_set.this["0"] : k => v if k != "tags" } : null
}

output "aws_sesv2_contact_list" {
  value = var.contact_list_enabled ? { for k, v in aws_sesv2_contact_list.this["0"] : k => v if k != "tags" && k != "last_updated_timestamp" && k != "topic" } : null
}

output "aws_sesv2_dedicated_ip_pool" {
  value = var.dedicated_ip_pool_enabled ? { for k, v in aws_sesv2_dedicated_ip_pool.this["0"] : k => v if k != "tags" } : null
}

output "aws_sesv2_email_identity" {
  value = { for k, v in aws_sesv2_email_identity.this["0"] : k => v if k != "tags" && k != "verified_for_sending_status" && k != "dkim_signing_attributes" }
}

output "aws_iam_user" {
  value = var.iam_user_enabled ? { for k, v in aws_iam_user.this["0"] : k => v if k != "tags" } : null
}

output "precomputed_aws_iam_user" {
  value = var.iam_user_enabled ? {
    name = var.iam_user.name
    path = var.iam_user.path
  } : null
}

output "aws_iam_access_key" {
  value = var.iam_user_enabled && var.iam_user.access_key_enabled ? { for k, v in aws_iam_access_key.this["0"] : k => v if k != "tags" && k != "status" && k != "secret" && k != "ses_smtp_password_v4" } : null
}

output "aws_ses_server_domain_name" {
  value = local.aws_ses_server_domain_name
}
